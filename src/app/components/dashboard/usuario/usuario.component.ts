import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Validators } from 'ngx-editor';
import { foroI, UsuarioRegistradoI } from 'src/app/interfaces/all-interface.interface';
import { ForosService } from 'src/app/services/foros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  listaRegistro!: UsuarioRegistradoI[];
  user!: UsuarioRegistradoI;
  hayForos: boolean = false;

  form!: FormGroup;

  listaForos!: foroI[];
  listaBorrador!: foroI[];
  hayborrador: boolean = true;

  constructor(private _usuarioSVC : UsuariosService,
              private fb : FormBuilder,
              private _forosSVC : ForosService,
              private router : Router) { 
    this.usuarioLogin()
    this.verBorrador()
    this.traerForos()

    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      correo: ['', [Validators.required]],
      usuario: ['', [Validators.required]],
      genero: ['', Validators.required],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required]
      });
        this.form.patchValue({
            nombre: this.user.nombre,
            usuario: this.user.usuario,
            genero: this.user.genero,
            correo: this.user.correo,
            pass1: this.user.pass1
          });
  }

  ngOnInit(): void {
  }

  usuarioLogin(){
    this.listaRegistro = this._usuarioSVC.obtenerLocalStorage()
    console.log(this.listaRegistro);

    const nombre = this._usuarioSVC.mostrarUser()
    let encontrado = this.listaRegistro.find(e => e.usuario === nombre)

    console.log(encontrado);
    if(encontrado === undefined){
      this.user = {
        nombre: 'no encontrado',
        correo:  'no encontrado',
        genero:  'no encontrado',
        usuario:  'no encontrado',
        pass1:  'no encontrado',
        pass2:  'no encontrado',
      }
    } else {
      this.user = encontrado
    }
  }

  
  cerrarSesion(){
    this._usuarioSVC.cerrarSesion();
  }

  traerForos(){

    if(localStorage.getItem("Foros") === null){
      console.log('no hay foros');
      return false
    } else {
      this.listaForos = this._forosSVC.obtenerLocalStorage()
      console.log(this.listaForos);
      this.hayForos = true

      const nombre = this._usuarioSVC.mostrarUser()
      this.listaForos = this.listaForos.filter(e => e.usuario === nombre)
      console.log(this.hayForos);
      
      return this.listaForos
    }
     

  }

  verBorrador(){
    if(sessionStorage.getItem('Foros') === null){
      this.hayborrador = false
      console.log('no hay borradores');

    } else {
      console.log('else');
      
      this.listaBorrador = JSON.parse(sessionStorage.getItem('Foros') || '');
    console.log(this.listaBorrador);
    
    let user = this._usuarioSVC.mostrarUser()
    console.log(user);

    let encontrados = this.listaBorrador.filter(e => e.usuario !== user)
    console.log(encontrados);

    let finder = this.listaBorrador.filter(data => {
      return data.usuario === user;
    });
    console.log(finder);
    this.listaBorrador = finder
    }
  }

  editarForo(number: number, foro: foroI){
    console.log(foro);
    console.log(foro.foroid);
    
    this.router.navigate(['/dashboard/crear-foro', foro.foroid])
  }

  editarForosPublicados(number: number, foro: foroI){
    // const list = this.traerForos();
    // console.log(list);

    console.log(foro);
    console.log(foro.foroid);

    this.router.navigate(['/dashboard/crear-foro', foro.foroid])
    
  }

}
